package main

import (
	"gitlab.com/bankex-task/bankex-client/cmd"
)

func main() {
	cmd.Execute()
}
