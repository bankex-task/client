package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	Port       string
	RateSource Source
}

type Source struct {
	URL string
}

// InitConfig instantiates config based on env variables
func InitConfig() *Config {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvPrefix("BANKEX_CLIENT")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	config := &Config{
		Port: viper.GetString("port"),
		RateSource: Source{
			URL: viper.GetString("source.url"),
		},
	}

	return config
}
