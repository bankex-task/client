# Client

## Usage

Clone client:
```
git clone https://gitlab.com/bankex-task/client.git

cd client
```

Run client with `pair` flag:
```
go run . rate --pair=ETHBTC,LTCBTC
```


