package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
)

var pair string

func init() {
	flag.StringVar(&pair, "pair", "", "pair to get price")
}

var rootCmd = &cobra.Command{
	Use:   "",
	Short: "Returns currency rates for pairs",
	Long:  `Gets currency rates for given pairs from https://api.binance.com`,
	Run: func(cmd *cobra.Command, args []string) {
		req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:3001/api/v1/rates?pairs=%s", pair), nil)
		if err != nil {
			log.Println(err)
			return
		}
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
			return
		}
		defer resp.Body.Close()
		result := map[string]string{}
		err = json.Unmarshal(body, &result)
		if err != nil {
			log.Println(err)
			return
		}

		pairList := strings.Split(pair, ",")
		for _, p := range pairList {
			fmt.Println(fmt.Sprintf("%s: %s", p, result[p]))
		}

	},
}

// Execute cli command
func Execute() {
	flag.Parse()
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
